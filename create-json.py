# encoding: utf-8
# Run this to create a json file from CSV files produced by glossika-text.
# Run with arguments: [source] [target] [files], for example:
# $ python create-json.py en fr GLOSSIKA-ENFR*.csv
# The result will be a json file that includes all the sentences in the CSV
# files. For each sentence, keys l1 and l2 contain source and target sentences,
# while l1-audio and l2-audio contain names of MP3 files. These are assumed to
# be in the format produced by glossika-split. For example, the 35th consecutive
# sentence in the CSV files is assumed to be present in files "en-0034.mp3" and
# "fr-0034.mp3" is en,fr are given as source and target on the command line. Note
# the -1 shift which corresponds to the way glossika-split numbers MP3 files.

from datetime import datetime
import csv
import json
import os
import re
import subprocess
import sys

if len(sys.argv) < 4:
  sys.exit("run with: src-lang target-lang csv-files")

l1 = sys.argv[1]
l2 = sys.argv[2]
dump = dict()
dump['l1'] = l1
dump['l2'] = l2
dump['_comment'] = "Created on %s by running: %s" %(
    datetime.now().strftime("%B %d, %Y"), sys.argv);
dump['sentences'] = []
curr_sentence = 1

for file in sys.argv[3:]:
  with open(file, "rb") as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      sentence = {'num': curr_sentence,
          'l1': row[0],
          'l1-audio': '%s-%04d.mp3' % (l1, curr_sentence-1),
          'l2': row[1],
          'l2-audio': '%s-%04d.mp3' % (l2, curr_sentence-1)
          };
      dump['sentences'].append(sentence)
      curr_sentence += 1
      
print json.dumps(dump, sort_keys=True, indent=2)

