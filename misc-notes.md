# Misc Notes

## How to package glossika-split.py for Windows.

Here's what I did.

1. Download [mp3splt for Windows](http://mp3splt.sourceforge.net/mp3splt_page/downloads.php) as an archive (NOTE not mp3splt-gtk). 

2. Only two files from there are actually needed for glossika-split to function: mp3splt.exe and libsplt_mp3-0.dll (NOTE not
the confusingly similar libmp3_splt-0.dll).

3. Install Python 2 for Windows if you don't have it yet (I did).

4. Install [PyInstaller](http://www.pyinstaller.org/). I used the [pip-Win](https://sites.google.com/site/pydatalog/python/pip-for-windows)
method specified in the [installation guide](https://pyinstaller.readthedocs.io/en/stable/installation.html) for Windows. I didn't bother
with virtual environments, and just executed `pip install PyInstaller` in the pip-Win window. Afterwards pyinstaller.exe is in the
scripts subfolder of the Python tree, so you may need to add that to PATH or invoke manually from there.

5. Now `pyinstaller -D glossika-split.py` and `pyinstaller -F glossika-split.py` work and create a folder or a self-containing
executable, respectively, but they don't have the two mp3splt files. In retrospect, I think it should work to simply copy these
files into the folder and zip everything up, but I didn't try that.

6. What I did: running pyinstaller creates glossika-split.spec. Edit this and replace binaries=None with 
`binaries=[('mp3split.exe','.'),('libsplt_mp3-0.dll','.')]`. Run `pyinstaller glossika-split.spec` to recreate directly from the
spec (the two mp3splt files should be in the same directory). Now pyinstaller will package the two files with everything else. But
it's still not working with the self-containing .exe route (`pyinstaller -F`), because mp3splt.exe can't find the DLL at runtime. I
don't want to spend more time debugging and solving this. The folder route (`pyinstaller -D`) works fine, although it's less pretty.
